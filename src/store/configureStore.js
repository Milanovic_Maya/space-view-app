import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import imagesReducer from '../reducers/reducer_images';
import searchReducer from '../reducers/reducer_search';
import errorReducer from '../reducers/reducer_error';

const logger = createLogger();

export default () => {
    return createStore(
        combineReducers({
            images: imagesReducer,
            searchInput: searchReducer,
            error:errorReducer
        }),
        applyMiddleware(thunk, logger)
    );
};