import { SEARCH_IMAGES } from "../actions/action_types";

const defaultState = {
    searchImages: ""
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case SEARCH_IMAGES:
            return {
                ...state,
                searchImages: action.searchValue
            }
        default:
            return state;
    }
}