import { ERROR_MESSAGE } from "../actions/action_types";

const defaultState = {
    status: false
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case ERROR_MESSAGE:
            return {
                ...state,
                status: action.error
            }
        default:
            return state;
    }
}