import { SET_IMAGE_LIST } from "../actions/action_types";


export default (state = [], action) => {
    switch (action.type) {
        case SET_IMAGE_LIST:
            return action.imageList;
        default:
            return state;
    }
};