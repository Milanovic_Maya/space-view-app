import { httpRequest } from './HTTPService';
import { BASE_URL } from '../shared/constants';

export default value => {
    console.log(`${BASE_URL}?q=${value}`);

    return httpRequest.get(`${BASE_URL}?q=${value}`)
}
