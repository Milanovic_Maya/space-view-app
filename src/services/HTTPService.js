import axios from 'axios';

class HTTP {
    get(url, options) {
        return axios.get(url, options)
    };
};

export const httpRequest = new HTTP();