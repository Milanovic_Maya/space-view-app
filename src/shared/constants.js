export const BASE_URL = 'https://images-api.nasa.gov/search';

// Images:
export const test_image = 'https://picsum.photos/200/300/?random';
export const spaceCat='https://static.giantbomb.com/uploads/square_medium/15/154910/2582672-space_cat_icon.jpg';


// Messages:
export const copyrigthFooterMessage = 'Ⓒ 2018 copyright Space Cats';
export const headerMessage = 'Space';
export const errorMessage='No results. Please, try again :)';