import React from 'react';
import { connect } from 'react-redux';
import { ImageCard } from './ImageCard/ImageCard';
import { ErrorMessage } from '../ErrorMessage/ErrorMessage';
import PropTypes from 'prop-types';

import './ImageCardList.css';

const ImageCardList = ({ imageList, error }) => (
    <div className="image-card-list">
        {imageList.length !== 0 && !error && imageList.map(image => <ImageCard image={image} key={image.id} />)}
        {error && <ErrorMessage />}
    </div>
)

const mapStateToProps = state => ({
    imageList: state.images,
    error: state.error.status
})

ImageCardList.propTypes = {
    imageList: PropTypes.array,
    error: PropTypes.bool
};

export default connect(mapStateToProps)(ImageCardList);