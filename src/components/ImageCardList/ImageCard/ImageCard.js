import React from 'react';
import PropTypes from 'prop-types';

import './ImageCard.css';

const ImageCard = ({ image }) => (
    <div className="image-card">
        <div className="image-card__thumb" style={{ backgroundImage: `url(${image.src})` }}></div>
        <div className="image-card__content">
            <h3 className="image-card__content__title">{image.title}</h3>
            <p className="image-card__content__date">{`Taken on : ${image.dateCreated}`}</p>
        </div>
    </div>
);

ImageCard.propTypes = {
    image: PropTypes.shape({
        src: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        dateCreated: PropTypes.string.isRequired
    })
};

export { ImageCard };