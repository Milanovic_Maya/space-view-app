import React from 'react';
import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import ImageCardList from '../ImageCardList/ImageCardList';
import Search from '../Search/Search';

import './App.css';

const App = () => (
  <div className="app">
    <Header />
    <div className="app__container">
      <Search />
      <ImageCardList />
    </div>
    <Footer />
  </div>
);

export default App;
