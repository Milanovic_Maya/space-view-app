import React from 'react';
import { headerMessage } from '../../shared/constants';

import './Header.css';

const Header = () => (
    <header className="header">
        <div className="header__thumb"></div>
        <h1 className="header__title">{headerMessage}</h1>
    </header>
);

export { Header };