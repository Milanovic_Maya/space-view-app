import React from 'react';
import { errorMessage } from '../../shared/constants';

import './ErrorMessage.css';

const ErrorMessage = () => (
    <div className="error">{errorMessage}</div>
);

export { ErrorMessage };
