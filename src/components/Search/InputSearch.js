import React from 'react';

import './Search.css';

const InputSearch = ({ searchValue, onSearchInput, onKeyDownSearch }) => (
    <div className="search">
        <label htmlFor="search-input">Browse NASA images</label>
        <input
            type="search"
            name="search"
            value={searchValue}
            onChange={onSearchInput}
            onKeyDown={onKeyDownSearch}
            autoComplete="off"
            id="search-input"
        />
    </div>
);

export { InputSearch };