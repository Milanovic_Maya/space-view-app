import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startSearch } from '../../actions/action_search';
import { setImageList } from '../../actions/action_images';
import { displayError } from '../../actions/action_error';
import { Card } from '../../entities/Card';
import PropTypes from 'prop-types';

import './Search.css';

class Search extends Component {
    state = {
        searchValue: ''
    }

    delay = null;

    onSearchInput = e => {
        const searchValue = e.target.value;
        if (this.delay) {
            clearTimeout(this.delay);
        };
        this.setState({ searchValue });
        if (searchValue) {
            this.delay = setTimeout(() => this.startSearch(searchValue), 1000);
        }
        if (!searchValue) {
            this.startSearch(searchValue);
        }
    };
    startSearch = searchValue => {
        this.props.startSearch(searchValue)
            .then(data => {
                if (data.length !== 0) {
                    const imageList = data.map(item => new Card(item.data[0], item.links[0].href));
                    return this.props.setImageList(imageList);
                }
                // if data.length===0 && no error is returned, status is 200... do something!!
            })
            .then(() => this.props.displayError(false))
            .catch(err => {
                console.log(err.message);
                return this.props.displayError(true)
            })
    };

    onKeyDownSearch = e => {
        const searchValue = e.target.value;
        if (searchValue) {
            if (e.keyCode === 13) {
                clearTimeout(this.delay);
                this.startSearch(searchValue);
            }
        }
    };

    render() {

        return (
            <div className="search">
                <label htmlFor="search-input">Browse NASA images</label>
                <input
                    type="search"
                    name="search"
                    value={this.state.searchValue}
                    onChange={this.onSearchInput}
                    onKeyDown={this.onKeyDownSearch}
                    autoComplete="off"
                    id="search-input"
                />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    startSearch: value => dispatch(startSearch(value)),
    setImageList: list => dispatch(setImageList(list)),
    displayError: err => dispatch(displayError(err))
});

Search.propTypes = {
    startSearch: PropTypes.func.isRequired,
    setImageList: PropTypes.func.isRequired,
    displayError: PropTypes.func.isRequired
};

export default connect(null, mapDispatchToProps)(Search);
