import moment from 'moment';

class Card {
    constructor(card, src) {
        this.src = src;
        this.title = card.title;
        this.description = card.description;
        this.id = card.nasa_id;
        this.dateCreated = moment(card.date_created).format(`Do MMMM YYYY`);
    }
}

export { Card };
