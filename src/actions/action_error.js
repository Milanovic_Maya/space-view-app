import { ERROR_MESSAGE } from "./action_types";

export const displayError = error => ({
    type: ERROR_MESSAGE,
    error
});