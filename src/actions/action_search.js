import { SEARCH_IMAGES } from "./action_types";
import getImages from '../services/apiService';

export const searchImages = searchValue => ({
    type: SEARCH_IMAGES,
    searchValue
});

export const startSearch = value => {
    return dispatch => {
        return getImages(value)
            .then(response => {
                console.log('Response from NASA:____=>',response);
                return response.data.collection.items
            })
    }
}