import { SET_IMAGE_LIST } from "./action_types";

export const setImageList = imageList => ({
    type: SET_IMAGE_LIST,
    imageList
});